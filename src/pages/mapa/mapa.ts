import { Component } from '@angular/core';
import { NavParams, ViewController } from 'ionic-angular';

@Component({
  selector: 'page-mapa',
  templateUrl: 'mapa.html',
})
export class MapaPage {

  lat: number = 51.678418;
  lng: number = 7.809007;

  constructor(public navParams: NavParams,
              private viewController: ViewController) {

    let ArrayCoordenadas = this.navParams.get("coordenadas").split(",");
    this.lat = Number(ArrayCoordenadas[0].replace("geo:",""));
    this.lng = Number(ArrayCoordenadas[1]);

    console.log(this.navParams.get("coordenadas"));
    console.log(this.lat, this.lng);
  }

  cerrar_modal(){
    this.viewController.dismiss();
  }

}
