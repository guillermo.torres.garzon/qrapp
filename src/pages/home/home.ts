import { Component } from '@angular/core';

//plugins de cordova
import { BarcodeScanner } from '@ionic-native/barcode-scanner';

//componentes
import {ToastController,Platform} from 'ionic-angular';

//servicios
import {HistorialProvider} from "../../providers/historial/historial";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(private barcodeScanner: BarcodeScanner,
              public toastCtrl:ToastController,
              private platform:Platform,
              private _historialProvider:HistorialProvider) { }
  scan()
  {
    console.log("Realizando scan...");

    if(!this.platform.is('cordova'))
    {
      //simular que se escaneo un codigo QR que contenia una pagina web
      //this._historialProvider.AgregarCodigoAlHistorial("http://proveee.com");

      //simular que se escaneo un codigo QR que contenia una ubicacion
      //this._historialProvider.AgregarCodigoAlHistorial("geo:20.7903963,-103.4511602");

      //simular que se escaneo un codigo QR que contenia una VCARD
      this._historialProvider.AgregarCodigoAlHistorial( `BEGIN:VCARD
VERSION:2.1
N:Kent;Clark
FN:Clark Kent
ORG:
TEL;HOME;VOICE:12345
TEL;TYPE=cell:67890
ADR;TYPE=work:;;;
EMAIL:clark@superman.com
END:VCARD` );
      return;
    }


    this.barcodeScanner.scan().then((barcodeData) => {
     // Success! Barcode data is here
     console.log("Result: " + barcodeData.text);
     console.log("Format: " + barcodeData.format);
     console.log("Cancelled: " + barcodeData.cancelled);

     if (barcodeData.cancelled == false &&  barcodeData.text != null)
     {
       this._historialProvider.AgregarCodigoAlHistorial(barcodeData.text);
     }

    }, (err) => {
        // An error occurred
        console.error("Error: ", err);
        this.MostrarError("Error: " + err);
    });
  }

  MostrarError(mensaje:string) {
   let toast = this.toastCtrl.create({
     message: mensaje,
     showCloseButton: true,
     closeButtonText: 'Ok'
     //duration: 3000
   });
   toast.present();
 }

}
