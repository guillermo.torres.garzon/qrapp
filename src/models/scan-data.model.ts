export class ScanData
{
  info:string; //lo que se lee de un QR es un string
  tipo:string; //nos servira para identificar que debemos hacer

  constructor(texto:string)
  {
    this.tipo = 'no definido';
    this.info = texto;

    if (texto.startsWith("http"))
    {
      this.tipo="http";
    }
    else if (texto.startsWith("geo"))
    {
      this.tipo="mapa";
    }
    else if (texto.startsWith("BEGIN:VCARD"))
    {
      this.tipo="contacto";
    }
  }
}
